import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {setName} from '../actions/nameActions';

class  Name extends React.Component{
  handleInputChange(event) {
    const value = event.target.value;
    this.props.setName(value);
  }
  render() {
    const name = this.props.nameReducer.name;
    return (
      <div className="name-wrapper">
        <input type="text" onChange={this.handleInputChange.bind(this)}/>
        <p>Hello {name}</p>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  setName,
}, dispatch);

const mapStateToProps = (state) => {
  return {
    nameReducer: state.nameReducer
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Name);