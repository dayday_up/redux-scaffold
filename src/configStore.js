import thunk from 'redux-thunk';
import {applyMiddleware, createStore} from 'redux';
import { rootReducer } from './reducers';
const configStore = (initialState) => createStore(
  rootReducer,
  initialState,
  applyMiddleware(thunk)
);

export default configStore({});