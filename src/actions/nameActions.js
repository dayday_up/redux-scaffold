export const setName = (name) => (dispatch) => {
  dispatch({
    type: "SET_NAME",
    name: name
  })
};